#ifndef MotionToPhotonServer_h
#define MotionToPhotonServer_h

#include "Arduino.h"
#include <WiFi.h>
#include <WebServer.h>
#include "SPIFFS.h"
#include "FS.h"

class MotionToPhotonServer
{
	private:
		unsigned int (* getPhotodiodeReading)();
		unsigned int (* getThreshold)();
		unsigned int (* getCurrentNumberOfResults)();
		String (* getStatus)();
		void (* setThreshold)(unsigned int);
		void (* setLedMoved)(bool);
		void (* startMeasure)(unsigned int);
		void (* abortMeasurement)();
	
		/* Put your SSID & Password */
		const char* ssid = "Calibratio2";  // Enter SSID here
		const char* password = "";  //Enter Password here

		/* Put IP Address details */
		IPAddress local_ip = IPAddress(192,168,1,1);
		IPAddress gateway = IPAddress(192,168,1,1);
		IPAddress subnet = IPAddress(255,255,255,0);
		WebServer server;

		/* File Read Buffer */
		#define file_buffer_length 8192
		char file_buffer[file_buffer_length];

		/* Result Buffer */
		unsigned long* result_buffer;
		unsigned int result_buffer_length;
		
		/* Pages */
		void pageSensor();
		void pageStatus();
		void pageSetThreshold();
		void pageGetThreshold();
		void pageSetLedDefault();
		void pageSetLedMoved();
		void pageMeasure();
		void pageResults();
		void pageIndex();
		void pageAbort();
		void pageNotFound();

		/* Utility */
		String readFile(String filename);
		
	public:
		/* Server functions */
		MotionToPhotonServer(unsigned long* result_buffer_, unsigned int result_buffer_length_,
			unsigned int (* getPhotodiodeReadingFunction)(),
			unsigned int (* getThresholdFunction)(),
			void (* setThresholdFunction)(unsigned int),
			String (* getStatusFunction)(),
			void (* startMeasureFunction)(unsigned int),
			unsigned int (* getCurrentNumberOfResultsFunction)(),
			void (* abortMeasurementFunction)(),
			void (* setLedMoved)(bool));
		void setup();
		void serve();
		
};

#endif
