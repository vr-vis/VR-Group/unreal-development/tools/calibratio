#include "MotionToPhotonServer.h"
#include "esp_task_wdt.h"

/* Enums */
enum Status {Ready = 0, Measuring = 1};
String Status_Text[2] = {"Ready", "Measuring"};
enum InternalStatus {DefaultReady, Moved};

/* Forward Declarations */
void startMeasurements(unsigned int numberOfSamples);
unsigned int getPhotoDiodeReading();
void setThresholdValue(unsigned int newThreshold);
unsigned int getThresholdValue();
String getStatus();
unsigned int getCurrentNumberOfResults();
void setLEDExternal(bool LedMoved);
void setLED(InternalStatus Status);
void abortMeasurement();

/* Hardware */
#define PhotoPin 32
#define Marker1Pin 16
#define Marker2Pin 17

/* Global Values */
long startTime = -1;
int triggerValue = 2500;
#define Timeout 10000000L
unsigned int requested_measurements = -1;
Status current_status = Status::Ready;
InternalStatus current_internal_status = InternalStatus::DefaultReady;

/* Result Storage */
#define result_buffer_length 10000
unsigned long result_buffer[result_buffer_length];
unsigned int result_number = 0;

/* Server */
MotionToPhotonServer server(result_buffer, result_buffer_length, getPhotoDiodeReading, getThresholdValue, setThresholdValue, getStatus, startMeasurements, getCurrentNumberOfResults, abortMeasurement, setLEDExternal);

/* Multicore */
TaskHandle_t TaskCore0;
TaskHandle_t TaskCore1;
const esp_task_wdt_config_t twdt_config = {
	.timeout_ms = 6000,
	.idle_core_mask = 0b00000011, // Disable on all cores
	.trigger_panic = false,
};

void core1( void * pvParameters ){
	while(true){	
		//just small delay while not measuring to not spend to much power in idle
		if(current_status == Status::Ready){
			delay(5);
			continue;
		}

		// Starting next measurement
		if(current_internal_status == InternalStatus::DefaultReady && result_number < requested_measurements){
			//Wait for screen to become black again and prevent false resets
			for(int i = 0; i < 10; i++){
				do{
					delay(100 + random(1,20));
				}while(getPhotoDiodeReading() < triggerValue);
			}
			
			//Switch marker location and start clock
			setLED(InternalStatus::Moved);
			startTime = micros();
		}

		//Stop/Record measurement value
		if(current_internal_status == InternalStatus::Moved	&& getPhotoDiodeReading() < triggerValue){
			result_buffer[result_number] = micros() - startTime;
			Serial.println("Time: " + String(result_buffer[result_number]));
			result_number++;
			setLED(InternalStatus::DefaultReady); //Reset to ready
			
			//Finished Measurements
			if(result_number >= requested_measurements){ 
				current_status = Status::Ready;
				requested_measurements = -1;
			}
		}

		// Timeout, Reset without Measurement
		if(current_internal_status == InternalStatus::Moved && (micros() - startTime) > Timeout){
			setLED(InternalStatus::DefaultReady);
		}
	}
}

void core0( void * pvParameters ){
	while(true){
		server.serve();
	}
}

void setup() {
	Serial.begin(115200);
		
	pinMode(PhotoPin, INPUT);
	pinMode(Marker1Pin, OUTPUT);
	pinMode(Marker2Pin, OUTPUT);

  	setLED(InternalStatus::DefaultReady);
  
	randomSeed(analogRead(PhotoPin) + analogRead(0)); //Initalize Random seed
	
	server.setup();

	esp_task_wdt_init(&twdt_config);
	xTaskCreatePinnedToCore(
                    core0,       // Task function.
                    "Server",    // name of task.
                    10000,      // Stack size of task
                    NULL,        // parameter of the task
                    2,           // priority of the task
                    &TaskCore0,  // Task handle to keep track of created task
                    0);          // pin task to core 0     
	delay(500);
	xTaskCreatePinnedToCore(
                    core1,       // Task function.
                    "Meter",     // name of task.
                    10000,      // Stack size of task
                    NULL,        // parameter of the task
                    2,           // priority of the task
                    &TaskCore1,  // Task handle to keep track of created task
                    1);          // pin task to core 1             
	delay(500);
	esp_task_wdt_deinit();
}

void loop(){} //Still needed for arduino core to work

void startMeasurements(unsigned int numberOfSamples){
	if(numberOfSamples > result_buffer_length) return;

	memset(result_buffer, 0l, result_buffer_length * sizeof(long)); //clear buffer
	result_number = 0;
	requested_measurements = numberOfSamples;

	setLED(InternalStatus::DefaultReady);
	current_status = Status::Measuring;
}

unsigned int getPhotoDiodeReading(){
	return analogRead(PhotoPin);
}

void abortMeasurement(){
	current_status = Status::Ready;
	requested_measurements = -1;

	setLED(InternalStatus::DefaultReady);
}

void setThresholdValue(unsigned int newThreshold){
	triggerValue = newThreshold;
}

void setLED(InternalStatus Status){
	if(Status == InternalStatus::DefaultReady){
		digitalWrite(Marker1Pin, HIGH);
		digitalWrite(Marker2Pin, LOW);
	} else {
		digitalWrite(Marker1Pin, LOW);
		digitalWrite(Marker2Pin, HIGH);
	}
	current_internal_status = Status;
}

void setLEDExternal(bool LedMoved){
	if(current_status == Status::Measuring) return; //Do not set while measuring
	setLED(LedMoved ? InternalStatus::Moved : InternalStatus::DefaultReady);
}

unsigned int getThresholdValue(){
	return triggerValue;
}

String getStatus(){
	return Status_Text[current_status];
}

unsigned int getCurrentNumberOfResults(){
	return max(result_number, 0u);
}
