#include "MotionToPhotonServer.h"

/* Server Setup */

	MotionToPhotonServer::MotionToPhotonServer(
		unsigned long* result_buffer_,
		unsigned int result_buffer_length_,
		unsigned int (* getPhotodiodeReadingFunction)(),
		unsigned int (* getThresholdFunction)(),
		void (* setThresholdFunction)(unsigned int),
		String (* getStatusFunction)(),
		void (* startMeasureFunction)(unsigned int),
		unsigned int (* getCurrentNumberOfResultsFunction)(),
		void (* abortMeasurementFunction)(),
		void (* setLedMovedFunction)(bool)
	) {
		getPhotodiodeReading = getPhotodiodeReadingFunction;
		getThreshold = getThresholdFunction;
		setThreshold = setThresholdFunction;
		getStatus = getStatusFunction;
		startMeasure = startMeasureFunction;
		abortMeasurement = abortMeasurementFunction;
		getCurrentNumberOfResults = getCurrentNumberOfResultsFunction;
		setLedMoved = setLedMovedFunction;
		result_buffer = result_buffer_;
		result_buffer_length = result_buffer_length_;
	}

	void MotionToPhotonServer::setup(){
		if(!Serial) Serial.begin(115200);
		if(!SPIFFS.begin()){
			Serial.println("An Error has occurred while mounting SPIFFS");
			return;
		}

		WiFi.softAP(ssid, password);
		WiFi.softAPConfig(local_ip, gateway, subnet);
		delay(5000);

		server.on("/", [this](){this->pageIndex();} );
		server.on("/measure", [this](){this->pageMeasure();});
		server.on("/results", [this](){this->pageResults();});
		server.on("/status", [this](){this->pageStatus();});
		server.on("/sensor", [this](){this->pageSensor();});
		server.on("/setThreshold", [this](){this->pageSetThreshold();});
		server.on("/getThreshold", [this](){this->pageGetThreshold();});
		server.on("/setLedDefault", [this](){this->pageSetLedDefault();});
		server.on("/setLedMoved", [this](){this->pageSetLedMoved();});
		server.on("/abort", [this](){this->pageAbort();});
		server.onNotFound([this](){this->pageNotFound();});

		server.begin();
		Serial.println("HTTP server started");
	}

	void MotionToPhotonServer::serve() {
		server.handleClient();
	}

/* Pages to Serve */	
	void MotionToPhotonServer::pageAbort(){
		abortMeasurement();
		server.send(200, "text/plain", "Ok."); 
	}
	
	void MotionToPhotonServer::pageStatus(){
		unsigned int results = getCurrentNumberOfResults();
		server.send(200, "text/plain", String(getStatus()) + ((results > 0) ? ": " + String(results):"") ); 
	}

	void MotionToPhotonServer::pageSetThreshold(){
		if(server.args() <= 0){
			server.send(400, "text/plain", "No parameters given. Give t=<threshold>");
			return;
		}
		unsigned int threshold = String(server.arg(0)).toInt();
		
		setThreshold(threshold);
		server.send(200, "text/plain", "Ok."); 
	}

	void MotionToPhotonServer::pageGetThreshold(){
		server.send(200, "text/plain", String(getThreshold())); 
	}

	void MotionToPhotonServer::pageSetLedDefault(){
		setLedMoved(false);
		server.send(200, "text/plain", "Ok."); 
	}

	void MotionToPhotonServer::pageSetLedMoved(){
		setLedMoved(true);
		server.send(200, "text/plain", "Ok."); 
	}

	void MotionToPhotonServer::pageSensor(){
		server.send(200, "text/plain", String(getPhotodiodeReading())); 
	}

	void MotionToPhotonServer::pageMeasure(){
		if(server.args() <= 0){
			server.send(400, "text/plain", "No parameters given. Give t=<times to measure>");
			return;
		}
		unsigned int timesToMeasure = String(server.arg(0)).toInt();
		startMeasure(timesToMeasure);
		Serial.println("Measuring " + String(timesToMeasure) + " times!");
		server.send(200, "text/plain", "Ok."); 
	}

	void MotionToPhotonServer::pageResults(){
		Serial.println("Client wants results page.");
		server.setContentLength(CONTENT_LENGTH_UNKNOWN);
		server.send(200, "text/plain", "");
		unsigned int currentNumberOfResults = getCurrentNumberOfResults();
		for(unsigned int i = 0; i < currentNumberOfResults; i++){
			server.sendContent(String(result_buffer[i]) + ((i != currentNumberOfResults-1) ? ",\n":""));
		}
		server.sendContent("");
	}

	void MotionToPhotonServer::pageIndex(){
		Serial.println("Client wants index page.");
		server.send(200, "text/html", readFile("index.html")); 
	}

	void MotionToPhotonServer::pageNotFound(){
		server.send(404, "text/plain", "Not found!");
	}

/* Utility */

	String MotionToPhotonServer::readFile(String filename){
		memset(file_buffer, '\0', file_buffer_length * sizeof(char)); //clear buffer
		File file = SPIFFS.open("/" + filename, "r");
		if (!file) return "";

		if(file.size() > file_buffer_length) return "Error Reading File.";
		unsigned int offset = 0;
		while(file.available()){
			file_buffer[offset] = file.read();
			offset++;
		}
		
		return String(file_buffer);
	}
